import os
import shutil

from PIL import Image

import pygame, sys
from pygame.locals import K_RIGHT, K_LEFT, QUIT

pygame.init()

global levelfile, filename, resolution, file_prefix, file_extension, width, height

levelfile = ''
width = -1
height = -1
filename = ''
resolution = 600 #(dpi)
file_prefix = ""
file_extension = "jpg"

BG_COLOR = (255, 255, 255)

def display_rect(screen, px, topleft, prior, pos, scale):
	global filename, resolution, file_prefix, file_extension

	# ensure that the rect always has positive width, height
	if topleft == None:
		#func was called without a topleft, which means clear the previous rectangle
		screen.fill(BG_COLOR)
		rect = px.get_rect()
		px = pygame.transform.scale(px, [int(rect.width / scale), int(rect.height / scale)])
		screen.blit(px, (rect[0] - pos[0], rect[1] - pos[1]))
		pygame.display.flip()

		return None

	#or, the usual situation, topleft is defined, so blit over the old rect and blit in the new.
	topleft = [(val / scale - pos[i]) for i , val in enumerate(topleft)]
	x, y = topleft
	bottomright = pygame.mouse.get_pos()
	width =  bottomright[0] - topleft[0]
	height = bottomright[1] - topleft[1]
	if width < 0:
		x += width
		width = abs(width)
	if height < 0:
		y += height
		height = abs(height)

	# eliminate redundant drawing cycles (when mouse isn't moving)
	current = x, y, width, height
	if not (width and height):
		return current
	if current == prior:
		return current

	# draw transparent box and blit it onto canvas
	rect = px.get_rect()
	px = pygame.transform.scale(px, [int(rect.width / scale), int(rect.height / scale)])
	screen.blit(px, (rect[0] - pos[0], rect[1] - pos[1]))
	im = pygame.Surface((width, height))
	im.fill((128, 128, 128))
	pygame.draw.rect(im, (32, 32, 32), im.get_rect(), 1)
	im.set_alpha(128)
	screen.blit(im, (x, y))
	pygame.display.flip()

	# return current box extents
	return (x, y, width, height)

def setup(px):
	global width, height

	# resize the image for the current screen's resolution
	px = pygame.transform.scale(px, (width, height))
	rect = px.get_rect()
	screen = pygame.display.set_mode((width, height))
	pygame.display.set_caption('Salaro Level Editor')
	screen.blit(px, px.get_rect())
	pygame.display.flip()
	return screen, px

def move(pos, scale, px, screen):
	x,y = pos
	rect = px.get_rect()
	screen.fill(BG_COLOR)
	px = pygame.transform.scale(px, [int(rect.width / scale), int(rect.height / scale)])
	screen.blit(px, (rect[0] - x, rect[1] - y))
	pygame.display.flip()
	#px.rect.topleft = pr.rect.topleft[0] - x, 

def main_loop():
	global levelfile, filename, resolution, file_prefix, file_extension, width, height

	levelfile = file_prefix + '.level'
	levelinfo = []

	# create folder for the files
	old_filename = filename
	filename = os.path.join(file_prefix, file_prefix + '_00.jpg')
	os.makedirs(file_prefix)
	shutil.copy(old_filename, filename)

	topleft = bottomright = prior = None
	n=0
	scale = 1
	pos = [0, 0]

	screen, px = setup(px = pygame.image.load(filename))
	curw = px.get_width()
	curh = px.get_height()

	out_idx = 1
	while n!=1:
		for event in pygame.event.get():

			if event.type == QUIT:
				lf = open(levelfile, 'w+')
				lf.write(str(out_idx) + '\n')
				lf.write(filename + '\n')
				for i in range(len(levelinfo) - 1, -1, -1):
					lf.write(levelinfo[i][0] + ' ' + str(levelinfo[i][1]) + ' ' + str(levelinfo[i][2]) + '\n')
				lf.close()
				sys.exit(0)

			if event.type == pygame.MOUSEBUTTONUP:
				if not topleft:
					topleft = [(val + pos[i]) * scale for i, val in enumerate(event.pos)]
				else:
					bottomright = [(val + pos[i]) * scale for i, val in enumerate(event.pos)]

		if topleft:
			#first corner has been selected
			prior = display_rect(screen, px, topleft, prior,pos,scale)
			if bottomright:
				# both selected
				left, upper, right, lower = (topleft + bottomright)
				if right < left:
					left, right = right, left
				if lower < upper:
					lower, upper = upper, lower

				im = Image.open(filename)
				imw, imh = im.size
				# inverse scale
				rscalew = float(imw) / curw
				rscaleh = float(imh) / curh
				# both images may have been resized, so real positions in the original "parent" image must be calculated for the highest resolution cut
				levelinfo.append((filename, int(left * rscalew), int(upper * rscaleh)))
				im = im.crop((int(left * rscalew), int(upper * rscaleh), int(right * rscalew), int(lower * rscaleh)))

				dpi = resolution
				outfilename = os.path.join(file_prefix, file_prefix + '_' + str(out_idx).zfill(2) + '.' + file_extension)
				im.save(outfilename, dpi = (dpi, dpi))
				out_idx += 1

				screen, px = setup(px = pygame.image.load(outfilename))
				## place the new image in the center
				#curw = px.get_width()
				#curh = px.get_height()
				#dx = (width - curw) / 2.
				#dy = (height - curh) / 2.
				#pos = (-dx, -dy)
				#move(pos,scale,px,screen)
				topleft = bottomright = prior = None
				prior = display_rect(screen, px, topleft, prior, pos, scale)

				lastw = curw
				lasth = curh
				filename = outfilename
                
	return

if __name__ == "__main__":
	filename = sys.argv[1]
	fname = filename.split('.')
	file_prefix = fname[0]
	file_extension = fname[1]
	resolution = int(600)

	info = pygame.display.Info()
	width = info.current_w
	height = info.current_h

	main_loop()

	pygame.display.quit()

