#!/usr/bin/python3
from numpy import array
from gi.repository import Gtk, Gdk, PangoCairo, Pango

class Area():
    def __init__(self, designer):
        self.d = designer
        self.pos = array((0.0, 0.0))
        self.size = array((400, 300))
        self.rot = 0.0
        self.selected = False

    def set_position(self, x, y):
        w, h = self.size

        if x < 0:
            x = 0.0
        if x + w > self.d.w:
            x = self.d.w - w

        if y < 0:
            y = 0.0
        if y + h > self.d.h:
            y = self.d.h - h
        self.pos = array((x, y))

    def is_in(self, x, y):
        x0, y0 = self.pos
        return (x > x0 - 5 and x < x0 + self.size[0] + 5 and
                y > y0 - 5 and y < y0 + self.size[1] + 5)

    def corner_prox(self, pos):
        def rel_pos(n, start, length):
            delta = n - start
            if delta < 10:
                return -1
            if (length - delta) < 10:
                return 1
            return 0

        if self.is_in(*pos):
            return tuple(map(rel_pos, pos, self.pos, self.size))
        else:
            return None

    def draw(self, cr, text, scale, selected):
        if selected:
            color = (0, 0, 0.7)
        else:
            color = (0, 0, 0)

        cr.save()

        cr.translate(*(self.pos * scale))
        cr.rotate(self.rot)

        cr.rectangle(0, 0, *(self.size * scale))

        cr.set_source_rgb(*color)
        cr.stroke()

        cr.move_to(3, 2)
        PangoCairo.show_layout(cr, text)

        cr.restore()

class Designer():
    def __init__(self, d_area, tree, selection):
        self.tree = tree
        self.d_area = d_area
        self.selection = selection

        self.scale = 1.0
        self.set_area(500, 500)

        font = Pango.FontDescription.from_string('7')
        self.text_layout = d_area.create_pango_layout('')
        self.text_layout.set_font_description(font)

        self.levels = [Area(self)]
        self.selected = None
        self.corner = None

        d_area.connect('draw', self.draw)

    def set_area(self, w, h):
        self.w = w
        self.h = h
        self.update_window_area()

    def zoom(self, widget):
        self.scale = widget.get_value() / 100.0
        self.update_window_area()

    def update_window_area(self):
        self.d_area.set_size_request(self.w * self.scale,
                                     self.h * self.scale)
        self.mark_redraw()

    def mark_redraw(self):
        window = self.d_area.get_window()
        w = window.get_width()
        h = window.get_height()
        self.d_area.queue_draw_area(0, 0, w, h)

    def draw(self, d_area, cr):
        cr.set_line_width(1)

        cr.rectangle(0, 0, self.w * self.scale, self.h * self.scale)
        cr.set_source_rgb(1, 1, 1)
        cr.clip_preserve()
        cr.fill()
        for i in range(len(self.levels)):
            if i:
                name = 'Level ' + str(i)
            else:
                name = 'Base'
            self.text_layout.set_text(name, -1)
            self.levels[i].draw(cr, self.text_layout,
                                self.scale, i == self.selected)

    def add_level(self, widget):
        if self.selected != None:
            i = len(self.levels)
            self.levels.insert(self.selected + 1, Area(self))
            self.d_area.queue_draw_area(0, 0, self.w, self.h)
            self.tree.append(None, ('Level ' + str(i), i))

    def remove_level(self, widget):
        if self.selected:
            del self.levels[self.selected]
            last = str(len(self.levels))
            self.tree.remove(self.tree.get_iter_from_string(last))
            self.mark_redraw()

    def select(self, selection):
        model, row = selection.get_selected()
        if row:
            self.selected, = model.get(row, 1)
        else:
            self.selected = None
        self.mark_redraw()

    cursors = {(-1, -1): Gdk.Cursor(Gdk.CursorType.TOP_LEFT_CORNER),
               (-1, 0): Gdk.Cursor(Gdk.CursorType.LEFT_SIDE),
               (-1, 1): Gdk.Cursor(Gdk.CursorType.BOTTOM_LEFT_CORNER),
               (0, -1): Gdk.Cursor(Gdk.CursorType.TOP_SIDE),
               (0, 0): None,
               (0, 1): Gdk.Cursor(Gdk.CursorType.BOTTOM_SIDE),
               (1, -1): Gdk.Cursor(Gdk.CursorType.TOP_RIGHT_CORNER),
               (1, 0): Gdk.Cursor(Gdk.CursorType.RIGHT_SIDE),
               (1, 1): Gdk.Cursor(Gdk.CursorType.BOTTOM_RIGHT_CORNER),
               None: None}

    def click(self, d_area, ev):
        pos = array((ev.x, ev.y)) / self.scale
        selected = None
        for i in reversed(range(len(self.levels))):
            if self.levels[i].is_in(*pos):
                selected = i
                break
        if selected != None:
            self.selection.select_path(str(i))
        else:
            self.selection.unselect_all()
        self.prev_pos = pos

    def move(self, d_area, ev):
        if self.selected == None:
            return

        area = self.levels[self.selected]
        pos = array((ev.x, ev.y)) / self.scale

        if ev.state & Gdk.ModifierType.BUTTON1_MASK:
            delta = self.prev_pos - pos
            if self.corner and self.corner != (0, 0):
                # Resizing
                # TODO: To be continued...
                pass
            else:
                # Moving
                area.set_position(*(-delta + area.pos))
                self.prev_pos = pos
                self.mark_redraw()
        else:
            self.corner = area.corner_prox(pos)
            self.d_area.get_window().set_cursor(Designer.cursors[self.corner])

def main():
    builder = Gtk.Builder()
    builder.add_from_file('interface.xml')

    tree = builder.get_object('levels_tree')
    tree.append(None, ('Base', 0))

    selection = builder.get_object('selection')

    d = Designer(builder.get_object('drawing'),
                 tree,
                 selection)

    handlers = {'quit': Gtk.main_quit,
                'zoom': d.zoom,
                'add_level': d.add_level,
                'remove_level': d.remove_level,
                'selection_changed': d.select,
                'move': d.move,
                'click': d.click}
    builder.connect_signals(handlers)

    selection.select_path('0')

    Gtk.main()

if __name__ == '__main__':
    main()
